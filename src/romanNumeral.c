#include <stdlib.h>
#include <string.h>

#include "romanNumeral.h"

enum RomanNum{M,D,C,L,X,V,I};

void parse(int* buffer, char* romanNumber)
{
	size_t i, size = strlen(romanNumber);
	for(i = 0; i<size; i++)
	{
		switch(romanNumber[i])
		{
		case 'M':
			buffer[M]++;
			break;
		case 'D':
			buffer[D]++;
			break;
		case 'C':
			if(romanNumber[i+1] == 'M')
			{
				buffer[D]++;
				buffer[C] += 4;
				i++;
			}
			else if(romanNumber[i+1] == 'D')
			{
				buffer[C] += 4;
				i++;
			}
			else
				buffer[C]++;
			break;
		case 'L':
			buffer[L]++;
			break;
		case 'X':
			if(romanNumber[i+1] == 'C')
			{
				buffer[L]++;
				buffer[X] += 4;
				i++;
			}
			else if(romanNumber[i+1] == 'L')
			{
				buffer[X] += 4;
				i++;
			}
			else
				buffer[X]++;
			break;
		case 'V':
			buffer[V]++;
			break;
		case 'I':
			if(romanNumber[i+1] == 'X')
			{
				buffer[V]++;
				buffer[I] += 4;
				i++;
			}
			else if(romanNumber[i+1] == 'V')
			{
				buffer[I] += 4;
				i++;
			}
			else
				buffer[I]++;
			break;
		}
	}
}

void unParse(char* buffer, int* counts)
{
	int i, index=0;
	for(i = 0; i<counts[M] ;i++)
		buffer[index++] = 'M';
	for(i = 0; i<counts[D] ;i++)
		buffer[index++] = 'D';
	
	if(counts[C] == 4)
	{
		if(counts[D] == 1)
		{
			index--;
			buffer[index++] = 'C';
			buffer[index++] = 'M';
		}
		else
		{
			buffer[index++] = 'C';
			buffer[index++] = 'D';
		}
	}
	else
		for(i = 0; i<counts[C] ;i++)
			buffer[index++] = 'C';
	
	for(i = 0; i<counts[L] ;i++)
		buffer[index++] = 'L';
	
	if(counts[X] == 4)
	{
		if(counts[L] == 1)
		{
			index--;
			buffer[index++] = 'X';
			buffer[index++] = 'C';
		}
		else
		{
			buffer[index++] = 'X';
			buffer[index++] = 'L';
		}
	}
	else
		for(i = 0; i<counts[X] ;i++)
			buffer[index++] = 'X';
	
	for(i = 0; i<counts[V] ;i++)
		buffer[index++] = 'V';
	
	if(counts[I] == 4)
	{
		if(counts[V] == 1)
		{
			index--;
			buffer[index++] = 'I';
			buffer[index++] = 'X';
		}
		else
		{
			buffer[index++] = 'I';
			buffer[index++] = 'V';
		}
	}
	else
		for(i = 0; i<counts[I] ;i++)
			buffer[index++] = 'I';

	buffer[index++] = '\0';
}

void aggregate(int * counts)
{
	counts[V] += counts[I]/5;
	counts[I] = counts[I]%5;
	
	counts[X] += counts[V]/2;
	counts[V] = counts[V]%2;
	
	counts[L] += counts[X]/5;
	counts[X] = counts[X]%5;
	
	counts[C] += counts[L]/2;
	counts[L] = counts[L]%2;
	
	counts[D] += counts[C]/5;
	counts[C] = counts[C]%5;
	
	counts[M] += counts[D]/2;
	counts[D] = counts[D]%2;
}

/*this method returns 0 on success or 1 if it cannot successfully borrow*/
int borrow(int * a, enum RomanNum position)
{
	if(position == M) return 1;
	
	if(a[position - 1] == 0 && borrow(a,position-1) == 1)
		return 1;
	
	a[position -1]--;
	if(position == I || position == X || position == C)
		a[position] += 5;
	else
		a[position] += 2;
	
	return 0;
}

/*this method returns 0 on success or non-zero if b is larger than a*/
int subtract(int * a, int * b)
{
	int result = 0;
	
	if(a[I] < b[I]) result += borrow(a,I);
	a[I] -= b[I];
	
	if(a[V] < b[V]) result += borrow(a,V);
	a[V] -= b[V];
	
	if(a[X] < b[X]) result += borrow(a,X);
	a[X] -= b[X];
	
	if(a[L] < b[L]) result += borrow(a,L);
	a[L] -= b[L];
	
	if(a[C] < b[C]) result += borrow(a,C);
	a[C] -= b[C];
	
	if(a[D] < b[D]) result += borrow(a,D);
	a[D] -= b[D];
	
	if(a[M] < b[M]) result += 1;
	a[M] -= b[M];
	
	return result;
}

char * RomanNumeralAdd(char * augend, char * addend)
{
	int romanNums[7] = {0,0,0,0,0,0,0};
	
	parse(romanNums, augend);
	parse(romanNums, addend);
	
	aggregate(romanNums);
	
	char * result = malloc(1000);
	unParse(result, romanNums);
	
	
	return result;
};

char * RomanNumeralSubtract(char * minuend, char * subtrahend)
{
	int romanNumsA[7] = {0,0,0,0,0,0,0};
	int romanNumsB[7] = {0,0,0,0,0,0,0};
	
	parse(romanNumsA,minuend);
	parse(romanNumsB, subtrahend);
	
	subtract(romanNumsA, romanNumsB);
	
	char * result = malloc(1000);
	
	unParse(result, romanNumsA);
	
	return result;
}
