CC = gcc
INC = inc/
ERROR_FLAGS = -Wall -Werror
TEST_FLAGS = `pkg-config --cflags --libs check`
CC_FLAGS = -iquote $(INC) -c $(ERROR_FLAGS) -std=c99
LINK_FLAGS = $(ERROR_FLAGS)
LIB_FLAGS = -shared $(ERROR_FLAGS)
FOLDERS = obj bin lib

#Setup Folders
obj:
	mkdir obj
	
bin:
	mkdir bin
	
lib:
	mkdir lib

#Compiling the object files
obj/romanNumeral.o: src/romanNumeral.c inc/romanNumeral.h
	@echo Compiling romanNumeral.o
	$(CC) $(CC_FLAGS) -fpic $< -o $@
	
obj/testRomanNumeral.o: test/testRomanNumeral.c inc/romanNumeral.h
	@echo Compiling testRomanNumneral.o
	$(CC) $(CC_FLAGS) $< -o $@

#Linking

bin/testRomanNumeral: obj/testRomanNumeral.o obj/romanNumeral.o
	@echo Linking Roman Numeral Test Suite
	$(CC) $(LINK_FLAGS) $^ -o $@ $(TEST_FLAGS)

lib/libRomanNumeral.so: obj/romanNumeral.o
	@echo Linking Roman Numeral Library
	$(CC) $(LIB_FLAGS) $^ -o $@

#Make Targets

clean:
	@$(RM) bin/*
	@$(RM) obj/*
	@$(RM) lib/*
	@echo "Cleaned all compiled output"
	
build: $(FOLDERS) lib/libRomanNumeral.so

test: $(FOLDERS) bin/testRomanNumeral
	@echo ------------Running Test Suite------------
	@bin/testRomanNumeral
	
