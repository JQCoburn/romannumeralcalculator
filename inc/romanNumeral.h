#ifndef ROMAN_NUMERAL_H
#define ROMAN_NUMERAL_H
/***********************
 * This is the Header file for the Roman Numeral Library
 * This Library will allow you to add and subtract roman numerals
 * 
 * Author: Josh Coburn
 * 10/13/2016
 * ********************/

extern char * RomanNumeralAdd(char * augend, char * addend);

extern char * RomanNumeralSubtract(char * minuend, char * subtrahend);

/*ROMAN_NUMERAL_H*/
#endif 
