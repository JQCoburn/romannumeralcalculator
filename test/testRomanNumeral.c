#include <check.h>
#include <stdlib.h>

#include "romanNumeral.h"

/*Empty test to make sure check is working*/
START_TEST(empty_test)
{
	
}
END_TEST

/*Addition Tests*/
START_TEST(basic_addition)
{
	char* result = RomanNumeralAdd("XX","II");
	ck_assert_str_eq(result,"XXII");
	free(result);
}
END_TEST

START_TEST(overlapped_addition)
{
	char* result = RomanNumeralAdd("XI","XI");
	ck_assert_str_eq(result,"XXII");
	free(result);
	result = RomanNumeralAdd("VI","XI");
	ck_assert_str_eq(result,"XVII");
	free(result);
}
END_TEST

START_TEST(basic_aggregate_addition)
{	/*tests the additions ability to aggregate into higher numbers*/
	char* result = RomanNumeralAdd("MDCCLXXVIII", "MDCCLXXVII");
	ck_assert_str_eq(result,"MMMDLV");
	free(result);
}
END_TEST

START_TEST(subtraction_notation_addition)
{
	char* result = RomanNumeralAdd("CCCXXXIII","CXI");
	ck_assert_str_eq(result,"CDXLIV");
	free(result);
	
	result = RomanNumeralAdd("DCCLXXVII","CCXXII");
	ck_assert_str_eq(result,"CMXCIX");
	free(result);
}
END_TEST

START_TEST(subtraction_notation_input_addition)
{
	char* result = RomanNumeralAdd("CDXLIV","CDXLIV");
	ck_assert_str_eq(result,"DCCCLXXXVIII");
	free(result);
	
	result = RomanNumeralAdd("CMXCIX","CMXCIX");
	ck_assert_str_eq(result,"MCMXCVIII");
	free(result);
}
END_TEST

/*Subtraction Tests*/
START_TEST(basic_subtraction)
{
	char* result = RomanNumeralSubtract("MMDDCCLLXXVVII","MDCLXVI");
	ck_assert_str_eq(result, "MDCLXVI");
	free(result);
}
END_TEST

START_TEST(borrow_subtraction)
{
	char* result = RomanNumeralSubtract("M","DCLXVI");
	ck_assert_str_eq(result, "CCCXXXIV");
	free(result);
}
END_TEST

START_TEST(borrow_subtraction_2)
{
	char* result = RomanNumeralSubtract("MDCLXV","DCLXVI");
	ck_assert_str_eq(result, "CMXCIX");
	free(result);
}
END_TEST

START_TEST(negative_subtraction)
{
	char* result = RomanNumeralSubtract("I","II");
	ck_assert_str_eq(result, "");
	free(result);
}
END_TEST

Suite * romanNum_suite(void)
{
	/*our Unit Test Suite*/
	Suite *s;
	/*Our Unit Test Groups*/
	TCase *tc_empty; /*the empty unit test group*/
	TCase *tc_add; /*the unit test group for addition*/
	TCase *tc_sub; /*the unit test group for subtraction*/
	
	/*Creating the Unit Test Suite*/
	s = suite_create("RomanNumeralCalcTest");
	
	/*Creating the empty unit tests*/
	tc_empty = tcase_create("Empty Test");
	tcase_add_test(tc_empty, empty_test);
	
	/*Creating the addition unit tests*/
	tc_add = tcase_create("Addition Tests");
	tcase_add_test(tc_add, basic_addition);
	tcase_add_test(tc_add, overlapped_addition);
	tcase_add_test(tc_add, basic_aggregate_addition);
	tcase_add_test(tc_add, subtraction_notation_addition);
	tcase_add_test(tc_add, subtraction_notation_input_addition);
	
	/*Creating the subtraction unit tests*/
	tc_sub = tcase_create("Subtraction Tests");
	tcase_add_test(tc_sub, basic_subtraction);
	tcase_add_test(tc_sub, borrow_subtraction);
	tcase_add_test(tc_sub, borrow_subtraction_2);
	tcase_add_test(tc_sub, negative_subtraction);
		
	suite_add_tcase(s,tc_empty);
	suite_add_tcase(s,tc_add);
	suite_add_tcase(s,tc_sub);
	
	return s;
}

int main (void)
{
	int number_failed;
	Suite *s;
	SRunner *sr;

	s = romanNum_suite();
	sr = srunner_create(s);

	srunner_run_all(sr, CK_NORMAL);
	number_failed = srunner_ntests_failed(sr);
	srunner_free(sr);
	return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
